package com.example.homework5beta1.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.interaction.GetAllPhotosUseCase
import com.example.domain.interaction.RefreshPhotosUseCase
import com.example.domain.model.Photos
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class ItemListViewModel @Inject constructor(
    val getAllPhotosUseCase: GetAllPhotosUseCase,
    val refreshPhotosUseCase: RefreshPhotosUseCase
) : ViewModel() {

    private val _photos = MutableLiveData<List<Photos>>()
    var photos: LiveData<List<Photos>> = _photos

    var _navigateToSelectedPhoto = MutableLiveData<Photos?>()
    val navigateToSelectedPhoto: LiveData<Photos?> = _navigateToSelectedPhoto


    init {
        refreshData()
        getPhotosFromRepository()
    }

    fun displayPhotoDetails(photo: Photos) {
        _navigateToSelectedPhoto.value = photo
    }

    fun displayPhotoDetailsComplete() {
        _navigateToSelectedPhoto.value = null
    }


    private fun refreshData() {
        viewModelScope.launch {
            refreshPhotosUseCase.invoke()
        }
    }

    fun getPhotosFromRepository() {
        viewModelScope.launch {
            photos = getAllPhotosUseCase.invoke()
        }
    }
}
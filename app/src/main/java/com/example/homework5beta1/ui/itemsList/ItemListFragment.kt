package com.example.homework5beta1.ui.itemsList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.domain.model.Photos
import com.example.homework5beta1.databinding.FragmentItemlistBinding
import com.example.homework5beta1.viewmodel.ItemListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ItemListFragment : Fragment() {

    private val viewModel by viewModels<ItemListViewModel>()
    //private val viewModel: ItemListViewModel by viewModels()

    private lateinit var binding: FragmentItemlistBinding

    private lateinit var listAdapter: ItemsPostAdapter
    //private val args: TasksFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentItemlistBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel

        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Set the lifecycle owner to the lifecycle of the view
        binding.lifecycleOwner = this.viewLifecycleOwner
        setupListAdapter()
        setupNavigation()
    }

    private fun setupListAdapter() {
        val viewModel = binding.viewmodel
        if (viewModel != null) {
            listAdapter = ItemsPostAdapter()
            listAdapter._navigateToSelectedPhoto1 = viewModel._navigateToSelectedPhoto
            binding.recyclerViewPosts.adapter = listAdapter
        }
    }

    private fun setupNavigation() {
        //LiveData<Photos?>
        viewModel.navigateToSelectedPhoto.observe(viewLifecycleOwner, {
            if (null != it) {
                openPhotoDetails(it)
                //viewModel.navigateToSelectedPhoto
            }
        })
    }

    private fun openPhotoDetails(photo: Photos) {
        val action = ItemListFragmentDirections.actionItemListFragmentToImageDetailFragment(photo)
        findNavController().navigate(action)
        viewModel.displayPhotoDetailsComplete()
    }
}
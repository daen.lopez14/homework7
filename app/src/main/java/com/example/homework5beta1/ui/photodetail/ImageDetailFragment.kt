package com.example.homework5beta1.ui.photodetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.homework5beta1.databinding.ImageDetailFragmentBinding
import com.example.homework5beta1.viewmodel.ImageDetailViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageDetailFragment : Fragment() {

    private val viewModel by viewModels<ImageDetailViewModel>()

    private lateinit var binding: ImageDetailFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ImageDetailFragmentBinding.inflate(inflater, container, false).apply {
            viewmodel = viewModel

        }
        return binding.root
    }
}
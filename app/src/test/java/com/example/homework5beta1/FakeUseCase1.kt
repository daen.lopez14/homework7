package com.example.homework5beta1

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.interaction.GetAllPhotosUseCase
import com.example.domain.model.Photos

class FakeUseCase1(var tasks: MutableLiveData<List<Photos>>) : GetAllPhotosUseCase {
    override suspend fun invoke(): LiveData<List<Photos>> = tasks
}

package com.example.homework5beta1.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.domain.model.Photos
import com.example.homework5beta1.FakeUseCase1
import com.example.homework5beta1.FakeUseCase2
import com.example.homework5beta1.MainCoroutineRule
import com.example.homework5beta1.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
class ItemListViewModelTest {

    private lateinit var getAllPhotosUseCase: FakeUseCase1
    private lateinit var refreshPhotosUseCase: FakeUseCase2
    private lateinit var tasks: MutableLiveData<List<Photos>>

    // Subject under test
    private lateinit var itemListViewModel: ItemListViewModel


    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    @Before
    fun setupViewModel() {
        //GIVEN a fresh viewModel
        tasks = MutableLiveData<List<Photos>>()
        getAllPhotosUseCase = FakeUseCase1(tasks)
        refreshPhotosUseCase = FakeUseCase2()
        itemListViewModel = ItemListViewModel(getAllPhotosUseCase, refreshPhotosUseCase)
    }

    @Test
    fun navigateAndDisplayPhotoDetails() {
        // When
        itemListViewModel.displayPhotoDetails(
            Photos(
                albumId = 1,
                id = 1,
                title = "title",
                url = "url",
                thumbnailUrl = "thumbnailurl"
            )
        )

        // Then
        val value = itemListViewModel.navigateToSelectedPhoto.getOrAwaitValue()
        assertThat(value, (CoreMatchers.not(nullValue())))
    }

    @Test
    fun getPhotosFromRepository() = mainCoroutineRule.runBlockingTest {
        // When
        val value = itemListViewModel.getPhotosFromRepository()

        // Then
        assertThat(value, (CoreMatchers.not(nullValue())))
    }
}


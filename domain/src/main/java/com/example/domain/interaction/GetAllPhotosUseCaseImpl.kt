package com.example.domain.interaction

import androidx.lifecycle.LiveData
import com.example.domain.model.Photos
import com.example.domain.repository.PhotoRepository

class GetAllPhotosUseCaseImpl(private val photoRepository: PhotoRepository) : GetAllPhotosUseCase {
    override suspend fun invoke(): LiveData<List<Photos>> = photoRepository.getAllPhotos()
}